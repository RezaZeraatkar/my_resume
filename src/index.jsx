import React from 'react';
import { render } from 'react-dom';

// My-Components
import App from './components/App/app';

// My-Styles
import './index.scss';

render(<App />, document.getElementById('root'));
