import React from 'react';

// Styles
import './skills.scss';

const Skills = () => (
  <div className="container">
    <p>
      <i className="fa fa-briefcase fa-fw margin-right large text-teal" />
      Designer
    </p>
    <p>
      <i className="fa fa-home fa-fw margin-right large text-teal" />
      Tehran, Iran
    </p>
    <p>
      <i className="fa fa-envelope fa-fw margin-right large text-teal" />
      r.zeraatkar1992@gmail.com
    </p>
    <p>
      <i className="fa fa-phone fa-fw margin-right large text-teal" />
      09354672750
    </p>
    <hr />
    <p className="large">
      <b>
        <i className="fa fa-asterisk fa-fw margin-right text-teal" />
    Skills
      </b>
    </p>
    <p>Web Design</p>
    <div className="round-xlarge light-grey font-small">
      <div className="container text-center round-xlarge teal" style={{ width: '90%' }}>90%</div>
    </div>
    <p>Web Design</p>
    <div className="round-xlarge light-grey font-small">
      <div className="container text-center round-xlarge teal" style={{ width: '90%' }}>90%</div>
    </div>
    <p>Web Design</p>
    <div className="round-xlarge light-grey font-small">
      <div className="container text-center round-xlarge teal" style={{ width: '90%' }}>90%</div>
    </div>
    <p>Web Design</p>
    <div className="round-xlarge light-grey font-small">
      <div className="container text-center round-xlarge teal" style={{ width: '90%' }}>90%</div>
    </div>
    <p>Web Design</p>
    <div className="round-xlarge light-grey font-small">
      <div className="container text-center round-xlarge teal" style={{ width: '90%' }}>90%</div>
    </div>
    <br />
    <p className="large">
      <b>
        <i className="fa fa-globe fa-fw margin-right text-teal" />
        Languages
      </b>
    </p>
    <p>English</p>
    <div className="round-xlarge light-grey font-small">
      <div className="text-center round-xlarge teal" style={{ width: '90%' }}>90%</div>
    </div>
  </div>
);

export default Skills;
